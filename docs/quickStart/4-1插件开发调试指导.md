# 插件开发调试指导
本章节将指导在SDK自带的图像分类识别样例中添加一个自定义插件并在IDE中单步调试的流程。  
在执行本样例前，应已成功远程部署并运行SDK自带的图像分类识别样例。  
[点击跳转代码样例](https://gitee.com/ascend/mindxsdk-referenceapps/tree/master/tutorials/mindx_sdk_plugin)

## 插件逻辑
![image.png](img/1623394204432.png 'image.png')

在本样例中，新构建的插件：
1. 从传入的MxpiBuffer中提取Metadata
>此时数据控制权转移到本插件
2. 从Metadata里复制一份MxpiObjectList中包含的MxpiClassList
3. 在其中添加一自定义classname
4. 回写至Metadata
5. 将修改后的MxpiBuffer送入steam中
>此时数据不再被本插件控制，且离开本插件进入下一插件

## 项目准备

**步骤1** 准备相关工程文件
在clion中创建新工程，该工程应该为样例的顶级目录（本样例中使用mxVision，mxManufacture替换对应名称即可，样例代码通用）

指定对应的远程开发目录  
![image.png](img/1623397820744.png 'image.png')
删除工程自带的main.cpp文件，并将已经之前成功部署并运行SDK自带的图像分类识别样例的文件夹下载至本地。
>**注意** model目录下需包含yolov3和resnet50模型文件

该步骤完成后的目录结构如下：  
![image.png](img/1623396772171.png 'image.png')

**步骤2** 测试图像分类识别样例  
添加环境变量  
![image.png](img/1623402187826.png 'image.png')
![image.png](img/1623401358361.png 'image.png')
修改顶层目录的cmakelist.txt，参考如下：
```cmake
cmake_minimum_required(VERSION 3.6)
project(plugin_sample)

#该语句中%MX_SDK_HOME%根据实际SDK安装位置修改，可通过在终端运行env命令查看
set(ENV{MX_SDK_HOME} %MX_SDK_HOME%)

add_subdirectory(./mxVision/C++)
add_subdirectory(./mindx_sdk_plugin)
```
随后加载该文件并build样例项目，并运行默认的图像分类识别样例  
![image.png](img/1623401056998.png 'image.png')  
正确时应该终端可输出识别到的图片内容  
![image.png](img/1623401140421.png 'image.png')

>若出现类似`[MpModelVisionInfer.cpp:731] [mxpi_modelinfer1][1016][Object, file or other resource doesn't exist] The postprocessing DLL [../../../lib/libresnet50postprocessor.so] does not exist.`的报错，请检查对应Sample.pipeline文件中`postProcessLibPath`项是否包含相对路径  
>![image.png](img/1623400877163.png 'image.png')  
尝试去掉或更改为绝对路径后再运行  
![image.png](img/1623400960176.png 'image.png')

**步骤3** 编译自定义插件
切换至插件并编译  
![image.png](img/1623401675765.png 'image.png')

成功后生成so文件`/mindx_sdk_plugin/lib/plugins/libmxpi_sampleplugin.so`，也可通过IDE操作下载到本地方便查看  
![image.png](img/1623401832093.png 'image.png')  
目录结构为  
![image.png](img/1623401950254.png 'image.png')

**步骤4** 运行带自定义插件的样例
修改上一步骤样例中main.cpp里94行中使用的pipeline文件路径，使其加载包含自定义插件的SamplePlugin.pipeline  
![image.png](img/1623401511515.png 'image.png')  
添加插件目录到GST_PLUGIN_PATH环境变量中  
![image.png](img/1623405875582.png 'image.png')  

正确时终端输出信息应该包含该字段  
![image.png](img/1623402363018.png 'image.png')

>若出现类似`[MpModelVisionInfer.cpp:731] [mxpi_modelinfer1][1016][Object, file or other resource doesn't exist] The postprocessing DLL [../../../lib/libresnet50postprocessor.so] does not exist.`的报错，请检查对应Sample.pipeline文件中`postProcessLibPath`项是否包含相对路径  
>![image.png](img/1623400877163.png 'image.png')  
尝试去掉或更改为绝对路径后再运行  
![image.png](img/1623400960176.png 'image.png')  

**步骤5** 单步调试插件
确认已设置为debug项目，toolchain指向代码所在的远程环境  
![image.png](img/1623749645831.png 'image.png')  
在插件的process函数中选择执行的语句打断点，并debug运行  
![image.png](img/1623406024516.png 'image.png')  
成功进入插件内部并命中断点时类似下图  
![image.png](img/1623406092822.png 'image.png')  
 >注意：需要调试时请注释cmakelist该行内容，这是用于安全编译设置的选项。但正式提交时请保持本行生效。
 ```cmake
 target_link_libraries(${TARGET_LIBRARY} -Wl,-z,relro,-z,now,-z,noexecstack -s)
 ```

 ### 注意事项
 `mindx_sdk_plugin/src/mxpi_sampleplugin/MxpiSamplePlugin.cpp`第141行起配置了pipeline中支持的props参数和默认值，如自行开发请按需求修改。
 ```c++
auto parentNameProSptr = std::make_shared<ElementProperty<string>>(ElementProperty<string>{
        STRING, "dataSource", "name", "the name of previous plugin", "mxpi_modelinfer0", "NULL", "NULL"});
auto descriptionMessageProSptr = std::make_shared<ElementProperty<string>>(ElementProperty<string>{
        STRING, "descriptionMessage", "message", "Description mesasge of plugin", "This is MxpiSamplePlugin", "NULL", "NULL"});
    
 ```
